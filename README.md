# CountCountries #

Application counting countries in a downloaded matrix

### Functionalities ###

* Download data

Using an Async Task, will download a matrix from an url.

* Structure

Application containing two activities with one fragment each.
The application supports dual pane mode and save the downloaded data on rotation change.

On the first screen, a progress and a text inform the user of the progression or display any errors.
On the second screen, downloaded data is displayed.

* Library

Library used to count countries in a matrix.
Quadratic algorithm running in O(N*M) with matrix[N][M].

The library was added in Android Studio and not with the jar file since I had this [error](http://stackoverflow.com/questions/24662801/bad-class-file-magic-or-version) and didn't have java 1.7 installed on my computer (30 minutes download). The jar file that would have been used can be found in /jarfile/countcountriesmatrix.jar .

* Unit test

Unit test showing two examples have been added to the project.

* Comment

Not every part of the code is commented since I ran out of time, mostly because of the jar issue.
I would be happy to answer any questions regarding my choices.

### Contributor ###

Made by Cedric Holtz: *holtz.cedric@gmail.com*