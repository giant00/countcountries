package com.holtz;

public class CountCountries {

	public int countCountries(Integer[][] countries) {
		int number = 0;
		if (countries == null) {
			return number;
		}

		for (int i = 0; i < countries.length; ++i) {
			for (int j = 0; j < countries[i].length; ++j) {
				if (countries[i][j] != null) {
					reformatArray(countries, i, j, countries[i][j]);
					number++;
				}
			}
		}
		return number;
	}

	private void reformatArray(Integer[][] countries, int i, int j, int currentNumber) {
		if(i < 0 || j < 0 || i >= countries.length || j >= countries[i].length || countries[i][j] == null)
			return;

		if (countries[i][j] == currentNumber) {
			countries[i][j] = null;
			reformatArray(countries, i, j + 1, currentNumber);
			reformatArray(countries, i, j - 1, currentNumber);
			reformatArray(countries, i + 1, j, currentNumber);
			reformatArray(countries, i - 1, j, currentNumber);
		}
	}
}
