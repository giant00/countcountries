package holtz.cedric.countcountries;

import com.holtz.CountCountries;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
	@Test
	public void addition_isCorrect() throws Exception {
		CountCountries countCountries = new CountCountries();

		// Test the dropbox example
		int result = countCountries.countCountries(new Integer[][] {
				{1,1,2,2,1},
				{3,1,2,3,3},
				{4,4,2,4,5},
				{1,4,4,4,2},
				{1,2,3,5,5}
		});
		assertEquals(result, 12);

		// Test the subject example
		result = countCountries.countCountries(new Integer[][] {
				{1, 2, 1},
				{1, 1, 2}
		});
		assertEquals(result, 4);

		// Test a non squared example
		result = countCountries.countCountries(new Integer[][] {
				{1,1,2,2},
				{3,1,2,3,3},
				{4,4,2,4,5},
				{1,4,4,4,2},
				{1}
		});
		assertEquals(result, 8);
	}
}