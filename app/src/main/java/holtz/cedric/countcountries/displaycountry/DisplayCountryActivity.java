package holtz.cedric.countcountries.displaycountry;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import holtz.cedric.countcountries.CountryResult;
import holtz.cedric.countcountries.R;

public class DisplayCountryActivity extends AppCompatActivity {

	private DisplayCountryFragment mDisplayFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// If we are in dual pane mode, finish this activity
		if (getResources().getBoolean(R.bool.has_two_panes)) {
			finish();
			return;
		}

		setContentView(R.layout.activity_display_country);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		mDisplayFragment = (DisplayCountryFragment) getSupportFragmentManager()
				.findFragmentById(R.id.fragmentDisaplyCountries);

		Intent intent = getIntent();
		if (intent != null) {
			CountryResult countryResult = new CountryResult(intent.getExtras());
			mDisplayFragment.loadResult(countryResult);
		}
	}

}
