package holtz.cedric.countcountries.displaycountry;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import holtz.cedric.countcountries.CountryResult;
import holtz.cedric.countcountries.R;

public class DisplayCountryFragment extends Fragment {

	private TextView mResult;
	private TextView mSize;

	public DisplayCountryFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_display_country, container, false);

		mSize = (TextView) view.findViewById(R.id.tvDisplaySize);
		mResult = (TextView) view.findViewById(R.id.tvDisplayResult);

		return view;
	}

	/**
	 * Load a CountryResult object on the UI
	 * @param result The countryResult object
	 */
	public void loadResult(CountryResult result) {
		String size = "Size: [" + result.getHeight() + ":" + result.getWidth() + "]";
		mSize.setText(size);
		mResult.setText(String.valueOf(result.getCount()));
	}
}
