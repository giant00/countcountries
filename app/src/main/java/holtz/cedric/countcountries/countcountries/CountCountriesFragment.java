package holtz.cedric.countcountries.countcountries;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.holtz.CountCountries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import holtz.cedric.countcountries.CountryResult;
import holtz.cedric.countcountries.Downloader;
import holtz.cedric.countcountries.R;

public class CountCountriesFragment extends Fragment implements View.OnClickListener {

	private static final String URL_DROPBOX = "https://dl.dropboxusercontent.com/u/15886318/mapdata.json";
	private static final String TAG = "CountCountriesFragment";

	private TextView mTextStatus;
	private ProgressBar mProgressStatus;
	private OnResultClickListener mOnResultListener;

	/**
	 * Interface definition for:
	 *   - a callback to be invoked when a result is obtained
	 */
	public interface OnResultClickListener {
		/**
		 * Callback called when a result is computed
		 * @param result CountryResult object
		 */
		void onResult(CountryResult result);

	}

	public CountCountriesFragment() {
	}

	public void setOnResult(OnResultClickListener onResultListener) {
		mOnResultListener = onResultListener;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_count_countries, container, false);

		mTextStatus = (TextView) view.findViewById(R.id.tvCountStatusDownload);
		mProgressStatus = (ProgressBar) view.findViewById(R.id.pbCountStatusDownload);
		Button mButtonDownload = (Button) view.findViewById(R.id.bCountDownloadData);

		mButtonDownload.setOnClickListener(this);

		return view;
	}

	@Override
	public void onClick(View v) {
		// Start the download when the button is clicked
		new DownloadUrlTask().execute(URL_DROPBOX);
	}

	private class DownloadUrlTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Display the start of work to the user
			mTextStatus.setText(getString(R.string.downloading));
			mTextStatus.setVisibility(View.VISIBLE);
			mProgressStatus.setVisibility(View.VISIBLE);
		}

		protected String doInBackground(String... urls) {
			try {
				return Downloader.downloadUrlToString(urls[0]);
			} catch (IOException e) {
				Log.e(TAG, "doInBackground: ", e);
				return null;
			}
		}

		protected void onPostExecute(String result) {
			mProgressStatus.setVisibility(View.INVISIBLE);
			if (result == null) {
				mTextStatus.setText(getString(R.string.error_downloading));
				return;
			}

			// Parse the result
			try {
				JSONObject jsonRootObject = new JSONObject(result);
				JSONArray spec = jsonRootObject.getJSONArray("spec");
				JSONArray countries = jsonRootObject.getJSONArray("data");

				int width = 0;
				int height = 0;
				if (spec.length() == 2) {
					width = spec.getInt(0);
					height = spec.getInt(1);
				}

				Integer[][] matrix = new Integer[countries.length()][];
				for(int i = 0; i < countries.length(); i++) {
					JSONArray jsonArray = countries.getJSONArray(i);
					matrix[i] = new Integer[jsonArray.length()];
					for(int j = 0; j < jsonArray.length(); j++) {
						matrix[i][j] = jsonArray.getInt(j);
					}
				}

				int countryCount = countCountryMatrix(matrix);

				CountryResult countryResult = new CountryResult(countryCount, width, height);
				if (mOnResultListener != null) {
					// Send the result to the activity
					mOnResultListener.onResult(countryResult);
				}

				mTextStatus.setVisibility(View.INVISIBLE);
			} catch (JSONException e) {
				Log.e(TAG, "onPostExecute: error parsing", e);
				mTextStatus.setText(getString(R.string.error_parsing));
			}
		}
	}

	/**
	 * Call the library to get the number of country in the matrix
	 * @param matrix The matrix containing countries
	 */
	private int countCountryMatrix(Integer[][] matrix) {
		CountCountries countCountries = new CountCountries();
		return countCountries.countCountries(matrix);
	}
}
