package holtz.cedric.countcountries.countcountries;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import holtz.cedric.countcountries.CountryResult;
import holtz.cedric.countcountries.R;
import holtz.cedric.countcountries.displaycountry.DisplayCountryActivity;
import holtz.cedric.countcountries.displaycountry.DisplayCountryFragment;


public class CountCountriesActivity extends AppCompatActivity implements CountCountriesFragment.OnResultClickListener {

	private boolean mIsDualPane;
	private DisplayCountryFragment mDisplayFragment;
	private CountryResult mCurrentResult;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		mIsDualPane = getResources().getBoolean(R.bool.has_two_panes);

		// Retrieve the fragments references
		CountCountriesFragment countryFragment = (CountCountriesFragment)
				getSupportFragmentManager().findFragmentById(R.id.fragmentCountCountries);
		mDisplayFragment = (DisplayCountryFragment)
				getSupportFragmentManager().findFragmentById(R.id.fragmentDisaplyCountries);

		// Initialize our fragment
		countryFragment.setOnResult(this);

		// Restore the current intent on orientation change
		restoreState(savedInstanceState);
	}

	/**
	 * Try to get result data from the Bundle
	 * @param savedInstanceState Bundle containing a result
	 */
	private void restoreState(Bundle savedInstanceState) {
		if (savedInstanceState == null) {
			return;
		}

		mCurrentResult = new CountryResult(savedInstanceState);

		// If we are in landscape mode, update display info
		if (mIsDualPane && mDisplayFragment != null) {
			mDisplayFragment.loadResult(mCurrentResult);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// Serialize our current result to handle configuration change
		if (mCurrentResult != null) {
			mCurrentResult.serializeBundle(outState);
		}
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onResult(CountryResult result) {
		mCurrentResult = result;
		if (mIsDualPane) {
			// Update the display data in the fragment
			mDisplayFragment.loadResult(result);
		} else {
			// Launch a new activity to display the result
			Intent intent = new Intent(this, DisplayCountryActivity.class);

			// Attach current result data to the intent
			Bundle bundle = new Bundle();
			result.serializeBundle(bundle);
			intent.putExtras(bundle);

			startActivity(intent);
		}
	}
}
