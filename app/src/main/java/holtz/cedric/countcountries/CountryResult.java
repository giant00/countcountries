package holtz.cedric.countcountries;

import android.os.Bundle;

/**
 * Created by holtz_c on 4/20/16.
 * CountCountries
 */
public class CountryResult {

	private static final String KEY_WIDTH = "width";
	private static final String KEY_HEIGHT = "height";
	private static final String KEY_COUNT = "count";

	private int mWidth;
	private int mHeight;
	private int mCount;

	public CountryResult(int count, int width, int height) {
		mCount = count;
		mWidth = width;
		mHeight = height;
	}

	public CountryResult(Bundle bundle) {
		mCount = bundle.getInt(KEY_COUNT);
		mWidth = bundle.getInt(KEY_WIDTH);
		mHeight = bundle.getInt(KEY_HEIGHT);
	}

	public void serializeBundle(Bundle bundle) {
		bundle.putInt(KEY_WIDTH, mWidth);
		bundle.putInt(KEY_HEIGHT, mHeight);
		bundle.putInt(KEY_COUNT, mCount);
	}

	public int getWidth() {
		return mWidth;
	}

	public int getHeight() {
		return mHeight;
	}

	public int getCount() {
		return mCount;
	}
}
